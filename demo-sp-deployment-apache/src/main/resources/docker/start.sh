#!/usr/bin/env bash

rsync -av /data/httpd/  /etc/httpd/
rsync -av /data/shibboleth/ /etc/shibboleth/
rsync -av /data/ssl/ /etc/pki/tls/certs/
chmod 600 /root/.ssh/*; chmod 600 /etc/pki/tls/certs/*

export LD_LIBRARY_PATH=/opt/shibboleth/lib64

chown shibd:shibd /etc/pki/tls/certs/metadatasigner.pem

sh /etc/shibboleth/shibd-redhat stop
sh /etc/shibboleth/shibd-redhat start
/usr/sbin/apachectl -DFOREGROUND