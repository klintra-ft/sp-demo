<html>
<head>
    <title>Talgildur Samleiki</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Tænastuveitari</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
    <link href="/css/fontawesome/css/all.min.css" rel="stylesheet">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/ts_entities.js" type="text/javascript"></script>
    <script src="/js/ts_eds.js" type="text/javascript"></script>
</head>

<body class="text-center">

<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">Tænastuveitari</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">

            </ul>
            <ul class="navbar-nav" >
                <li class="smt-2 mt-md-0 mr-2">
                    <a id="sessionuser" href="/user/" class="d-none nav-link"><i class="fa fa-user"></i></a>
                </li>
                <li class="smt-2 mt-md-0">
                    <a id="sessionlogout" href="/Shibboleth.sso/Logout" class="d-none nav-link"><i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>

            <script type="text/javascript">
                $.get("/Shibboleth.sso/Session",function(data,status) {
                    if(data.indexOf("A valid session was not found")>=0) {
                        $("#sessionuser").addClass("d-none");
                        $("#sessionlogout").addClass("d-none");
                    }
                    else {
                        $("#sessionuser").removeClass("d-none");
                        $("#sessionlogout").removeClass("d-none");
                    }
                });
            </script>
        </div>
    </nav>
</header>



<!--<nav class="nav nav-masthead justify-content-end">-->
    <!--<a href="/Shibboleth.sso/Logout" class="btn btn-sm btn-secondary">Útrita</a>-->
<!--</nav>-->

<!-- Begin page content -->
<main role="main" class="container mt-5">
    <div class="container">
    <div class="jumbotron">
        <h1><i class="fa fa-ban red"></i>Innritan miseydnaðist</h1>
        <p class="lead">Trupuleikar eru við at rita inn á <em><span>Talgildan Samleika</span></em>.</p>
        <p><?php
            if (isset($_GET['errorText'])) {
                echo $_GET['errorText'];
            }
            ?>
        </p>
    </div>
</div>

<div class="container">
    <div class="body-content">
        <div class="row">
            <div class="col-md-6">
                <h2>Hvat hendi?</h2>
                <p class="lead">Tín innritan til sjálvgreiðsluna hjá Talgildum Samleika miseydnaðist. Tað kan verða orsaka av skeivum loyniorðið ella útgingum samleika.</p>
            </div>
            <div class="col-md-6">
                <h2>Hvat kann eg gera?</h2>
                <p> since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchan</p>
            </div>
        </div>
    </div>
</div>

</main>

<footer class="footer">
    <div class="container">
        <span class="text-muted">Talgildu Føroyar, <a href="http://www.talgildu.fo">www.talgildu.fo</a></span>
    </div>
</footer>

<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>