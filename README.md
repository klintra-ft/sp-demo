# Talgildur Samleiki
## Shibboleth-based templates of Service Provider configurations

This repository contains 2 examples of Shibboleth-based service provider configurations which work with
the Talgildur Samleiki test environment. 

The configuration assumes working knowledge of the [SAML 2.0 protocol](http://docs.oasis-open.org/security/saml/Post2.0/sstc-saml-tech-overview-2.0.html) and the Shibboleth Service Provider documentation as 
presented on the [Shibboleth wiki](https://shibboleth.atlassian.net/wiki/spaces/SP3/overview). This must be understood before a final 
service provider configuration is prepared for production as these templates do not utilize all features and options which 
are provider by the application.

Furthermore it is required that any service provider for Talgildur Samleiki 
is configured according to the specifications found in the following documents
on https://repository.samleiki.fo/:

* [Samleikin Attribute Specification V1.0](https://repository.samleiki.fo/assets/SamleikinAttributeSpecification1-0.pdf)
* [Samleikin Deployment Profile](https://repository.samleiki.fo/assets/SamleikinDeploymentProfile1-0.pdf)
* [Samleikin Registry of Identifiers](https://repository.samleiki.fo/assets/SamleikinRegistryofIdentifiers1-0.pdf)

As mentioned above this repository contains two example configurations. 

The examples have the following characteristics:

* A template which creates a Service Provider for Apache in Docker container
* A template for Windows IIS based on Shibboleth Service Provider.

All the configurations are registered with the same entityid "https://demo.test1.samleiki.fo/shibboleth" the 
test-metadata file found on the following link: https://innrita.test.samleiki.fo/metadata.xml.

All the configurations therefore also share the same key-pairs which are only to be used for test purposes. 
The keys must be replace before the service
providers can be registered in the staging and production environments.

It is possible to run the service provider on a local computer which eases the process of testing and
verifying the correctness of the service provider setup and configuration before deploying them to a server.

Once the service provider has been correctly configured and setup following these guides, it is possible to login 
to the Talgildur Samleiki test-environment.

## Metadata

The service provider and identity provider need to exchange metadata in order to build trust
in the federation. In short the metadata incorporates information about an _entity_ in the federation, such as
relevant endpoints, signing and encryption keys. This information is used when it comes to exchanging 
sensitive information over the web in a secure manner as required and specified by the SAML 2 protocol. 

The metadata for the identity provider in Talgildur Samleiki test environment is available in the link below:

* Talgildur Samleiki Test Identity Provider Metadata: https://innrita.test.samleiki.fo/metadata.xml

The metadata of the identity provider describes the federation, and is digitally signed by a private key held by the 
identity provider itself. It is possible to verify the signature of the metadata by using the corresponding public key
which can be found in the link below:

* Public key of the Test Identity Provider:

[metadatasigner.pem](./demo-sp-deployment-apache/src/main/resources/config/sp/ssl/metadatasigner.pem)

The metadadata for the identity provider also specifies the _attributes_ which are requested by each of the 
service providers in the federation, e.g.:

* A unique identification of the service provider, i.e. the `Entity ID` 
* Display text which describes the name, purpose, and a link to a logo
* The public key which is required by the SAML 2 protocol for verifying the validity of messages sent back and forth 
in the federation between the parties
* URL endpoints, e.g. used for login and logout
 
# Sample Service Providers

Below are links to the service provider templates which are located in separate sub-folders in this repository.

## Apache in Docker

For a detailed description on how to run a Shibboleth Service Provider using Apache, follow the link below:
 
* [Shibboleth Service Provider in Apache](./demo-sp-deployment-apache/README.md)

## Windows IIS

For a detailed description on how to run a Shibboleth Service Provider 3.0 in Windows using Internet Information Services
(IIS), follow the link below:
 
* [Shibboleth Service Provider in Windows (IIS)](./demo-sp-deployment-windows-iis-3_0/README.md)

## Configuration and Limitations

Since these guides are limited only to describing the setup of a sample service provider, not all configuration features
and capabilities available in the Shibboleth SAML 2 service provider are described.

However, some of the key _features_ of the Service Provider are:

* Only accepting signed _assertions_ from the identity provider
* Only accepting encrypted _assertions_
* Requiring digitally signed metadata only
* Description of required _attributes_
* Single Log-out

## Generating Service Provider key pair using OpenSSL

This section describes the process of creating a key pair for a Service Provider using OpenSSL. OpenSSL must be 
installed on the machine in order to generate the key pair  (https://www.openssl.org/).

Executing the following OpenSSL command will generate a 4096 bit RSA key pair which is valid for 5 years
(i.e. 1825 days). The **-subj** parameter can be adjusted to reflect the specific Service Provider.

```
> openssl req -x509 -newkey rsa:4096 -nodes -keyout sp-key.pem -out sp-cert.pem -days 1825 -subj "/C=FO/ST=Torshavn/O=Demo/OU=Org/CN=www.demo.test1.samleiki.fo"
```

This is the result of running the OpenSSL command:

```
> openssl req -x509 -newkey rsa:4096 -nodes -keyout sp-key.pem -out sp-cert.pem -days 1825 -subj "/C=FO/ST=Torshavn/O=Demo/OU=Org/CN=www.demo.test1.samleiki.fo"
Generating a RSA private key
.......................................................................................................................++++
.....................++++
writing new private key to 'sp-key.pem'
-----
> dir


    Directory: C:\sp-keys


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        18-03-2021     08:30           2050 sp-cert.pem
-a----        18-03-2021     08:30           3468 sp-key.pem

```

In order to create a certificate containing the private key and public key, e.g. as a backup media of the keys, 
a password protected **.p12** file can be generated. 

Running the following OpenSSL command will create a **cert.p12** file - make sure to remember the *Export Password*: 

```
> openssl pkcs12 -export -inkey sp-key.pem -in sp-cert.pem -out cert.p12
```

This is the result of running the OpenSSL command for creating a **.p12**:

```
> openssl pkcs12 -export -inkey sp-key.pem -in sp-cert.pem -out cert.p12
Enter Export Password: ********
Verifying - Enter Export Password: ********
> dir


    Directory: C:\sp-keys


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        19-03-2021     08:30           4205 cert.p12
-a----        19-03-2021     08:29           2050 sp-cert.pem
-a----        19-03-2021     08:29           3324 sp-key.pem

```

## Extract public- and private key from P12 file

Before a service provider can be registered as a new separate entity in the metadata a unique key pair must be 
generated. The exact steps of this process varies depending on the execution environment (test, staging or production).

But typically the key pair is received as a p12-keystore file. 
To extract the keypair from the P12 keystore, the following commands should be executed.

##### Private key

    openssl pkcs12 -in file.p12 -nocerts -nodes -out privateKey.pem

##### Public key

    openssl pkcs12 -in file.p12 -clcerts -nokeys -out publicCert.pem

## Test SSN

The follow test SSN/SSN's can be used to authenticate in the test environment. These bypass the requirement to authenticate on the Samleikin mobile app, which can be convenient during testing.

* 100173999