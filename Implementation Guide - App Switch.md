# Implementation Guide - App Switch

## Context
To improve the user experience when authenticating with Samleikin using a service provider mobile app, it is desired to automatically switch to the Samleikin app for authentication and back again to the service provider app when the authentication is completed.
This document explains the details required for a service provider app to implement this feature. 

## Concept
App Switch functionality is achieved with the following flow.

![Flow](img/AppSwitchFlow.png)


|Step|Initiator|Action|Data|Result|
|-----|---------|------|----|------|
|1|SP App|The service provider app opens a protected url with an embedded webview and provides the callback uri in the template-query-parameter.|{sp-domain-and-and-path}?template={template-query-parameter}|User is redirected to the loginhandler in the IdP.|
|2|SP App|The service provider app detects when the button "Open Samleikin" is clicked in the webview and calls a uri with a schema Samleikin has registered to handle.|samleikin://|Samleikin app is opened.|
|3|Samleikin App|The Samleikin app polls for the auth request.||User is asked to authenticate.|
|4|Samleikin App|After the authentication is completed, the Samleikin app will attempt to call the callback uri.|appCallbackUri. For example serviceproviderapp://|SP app is opened.|

### Step 1
When the service provider app request a protected resource, it is the responsibility of the service provider backend to generate an AuthnRequest and call the Samleikin IdP. Attributes are defined in the AuthnRequests in the samlp:Extensions node as following:

	<samlp:Extensions>
		<ts-ext:extattrs xmlns:ts-ext="http://samleiki.fo/login-attr-extensions">
			<ts-ext:redirectURI>
				{sp-schema://}
			</ts-ext:redirectURI>
		</ts-ext:extattrs>
	</samlp:Extensions>
	
For the purpose of this guide we will assume that the service provider is based on Shibboleth SP. The Shibboleth service provider is able to receive a template query parameter when the user is requesting a protected resource. The template query parameter must consist of a url-encoded and base64-encoded template AuthnRequest. (I.e. urlEncode(Base64(authnrequest))). The Shibboleth service provider will extract the samlp:Extensions node from the template authnrequest and inject it into the final AuthnRequest being sent to the IdP.

	<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="foo" Version="2.0" IssueInstant="2012-01-01T00:00:00Z">
	    <samlp:RequestedAuthnContext Comparison="exact" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">
	        <saml:AuthnContextClassRef>https://federation.org/ac/type1</saml:AuthnContextClassRef>
	        <saml:AuthnContextClassRef>https://federation.org/ac/type2</saml:AuthnContextClassRef>
	    </samlp:RequestedAuthnContext>
	    <samlp:Extensions>
	        <ts-ext:extattrs xmlns:ts-ext="http://samleiki.fo/login-attr-extensions">
	            <ts-ext:redirectURI>
	                {sp-schema://}
	             </ts-ext:redirectURI>
	          </ts-ext:extattrs>
	    </samlp:Extensions>
	</samlp:AuthnRequest>

For implementation/testing purposes, the following links are useful:

[https://www.samltool.com/base64.php](https://www.samltool.com/base64.php)

[https://www.samltool.com/url.php](https://www.samltool.com/url.php)

The encoded Template AuthnRequest above is:
PHNhbWxwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiBJRD0iZm9vIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxMi0wMS0wMVQwMDowMDowMFoiPg0KCTxzYW1scDpSZXF1ZXN0ZWRBdXRobkNvbnRleHQgQ29tcGFyaXNvbj0iZXhhY3QiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPg0KwqDCoMKgwqAJPHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BaHR0cHM6Ly9mZWRlcmF0aW9uLm9yZy9hYy90eXBlMTwvc2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj4NCsKgwqDCoMKgwqDCoMKgwqA8c2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj5odHRwczovL2ZlZGVyYXRpb24ub3JnL2FjL3R5cGUyPC9zYW1sOkF1dGhuQ29udGV4dENsYXNzUmVmPg0KwqDCoMKgwqA8L3NhbWxwOlJlcXVlc3RlZEF1dGhuQ29udGV4dD4NCgk8c2FtbHA6RXh0ZW5zaW9ucz4NCgkJPHRzLWV4dDpleHRhdHRycyB4bWxuczp0cy1leHQ9Imh0dHA6Ly9zYW1sZWlraS5mby9sb2dpbi1hdHRyLWV4dGVuc2lvbnMiPg0KCQkJPHRzLWV4dDpyZWRpcmVjdFVSST4NCsKgwqDCoMKgCQkJe3NwLXNjaGVtYTovL30NCsKgwqDCoAkJCTwvdHMtZXh0OnJlZGlyZWN0VVJJPg0KwqDCoAkJPC90cy1leHQ6ZXh0YXR0cnM%2BDQoJPC9zYW1scDpFeHRlbnNpb25zPg0KPC9zYW1scDpBdXRoblJlcXVlc3Q%2B

There is a whitelist of allowed sp-schema for each service provider. It is therefor necessary to register your sp-schema with Samleikin.

### Step 2
Samleikin v.4.1.1 or above is registered to handle the uri schema samleikin. Because it is not possible on all devices to open an external app directly from link in a webview, it is necessary to register a navigation listener on the webview.
If the webviews attempts to navigate to samleikin://, the service provider app should request the OS to open the Samleikin app.
Implementation details are given in the iOS and Android sections.

### Step 3
The Samleikin app polls for new authentication request. When it receives a request, it extracts the appCallbackUri payload. No additional efforts from the service provider are need in this step.

### Step 4
When the Samleiki app has completed the authentication, it will attempt to call the appCallbackUri.
The appCallbackUri must have the following format {sp-schema}:// and the service provider app must be registered to handle this schema.
Implementation details are given in the iOS and Android sections.

## App implementation details
This section will describe implementation details for the iOS and Android service provider app implementation.
For the purpose of this guide, we will assume the following:

* IdP url for a protected resource: [https://case1.staging.klintraft.fo/Shibboleth.sso/Login](https://case1.staging.klintraft.fo/Shibboleth.sso/Login)
* SP app schema: **klintrademo**

Given the SP app schema, we define a Template AuthnRequest


	<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="foo" Version="2.0" IssueInstant="2012-01-01T00:00:00Z">
	    <samlp:RequestedAuthnContext Comparison="exact" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">
	        <saml:AuthnContextClassRef>https://federation.org/ac/type1</saml:AuthnContextClassRef>
	        <saml:AuthnContextClassRef>https://federation.org/ac/type2</saml:AuthnContextClassRef>
	    </samlp:RequestedAuthnContext>
	    <samlp:Extensions>
	        <ts-ext:extattrs xmlns:ts-ext="http://samleiki.fo/login-attr-extensions">
	            <ts-ext:redirectURI>klintrademo://</ts-ext:redirectURI>
	          </ts-ext:extattrs>
	    </samlp:Extensions>
	</samlp:AuthnRequest>
	
Using the tools from [https://samltool.com](https://samltool.com) we encode the Template AuthnRequest:
PHNhbWxwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiBJRD0iZm9vIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxMi0wMS0wMVQwMDowMDowMFoiPg0KCTxzYW1scDpSZXF1ZXN0ZWRBdXRobkNvbnRleHQgQ29tcGFyaXNvbj0iZXhhY3QiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPg0KwqDCoMKgwqAJPHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BaHR0cHM6Ly9mZWRlcmF0aW9uLm9yZy9hYy90eXBlMTwvc2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj4NCsKgwqDCoMKgwqDCoMKgwqA8c2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj5odHRwczovL2ZlZGVyYXRpb24ub3JnL2FjL3R5cGUyPC9zYW1sOkF1dGhuQ29udGV4dENsYXNzUmVmPg0KwqDCoMKgwqA8L3NhbWxwOlJlcXVlc3RlZEF1dGhuQ29udGV4dD4NCgk8c2FtbHA6RXh0ZW5zaW9ucz4NCgkJPHRzLWV4dDpleHRhdHRycyB4bWxuczp0cy1leHQ9Imh0dHA6Ly9zYW1sZWlraS5mby9sb2dpbi1hdHRyLWV4dGVuc2lvbnMiPg0KCQkJPHRzLWV4dDpyZWRpcmVjdFVSST5rbGludHJhZGVtbzovLzwvdHMtZXh0OnJlZGlyZWN0VVJJPg0KwqDCoAkJPC90cy1leHQ6ZXh0YXR0cnM%2BDQoJPC9zYW1scDpFeHRlbnNpb25zPg0KPC9zYW1scDpBdXRoblJlcXVlc3Q%2B

### iOS

#### Step 1 Check if Samleikin is installed on the same device and add the template query parameter when requesting the protected resource

App switch is only applicable, if the Samleikin app is installed on the same device as the service provider app. To check for this we need the following:
The Info.plist file should contain the following lines to gain permission to check if there is any app registered to handle the samleikin schema:

	<key>LSApplicationQueriesSchemes</key>
	 <array>
	 <string>samleikin</string>
	</array>
	
Then in viewDidLoad of the ViewController referencing the WebView (wv):

	override func viewDidLoad(){
	    super.viewDidLoad()
	    ...            
	    //Shibboleth IdP uses javascript
	    wv.configuration.preferences.javaScriptEnabled = true
	 
	    //The viewcontroller that contains the webview is set as the navigation delegate    
	    wv.navigationDelegate = self
	 
	    let templateParameter = "PHNhbWxwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiBJRD0iZm9vIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxMi0wMS0wMVQwMDowMDowMFoiPg0KCTxzYW1scDpSZXF1ZXN0ZWRBdXRobkNvbnRleHQgQ29tcGFyaXNvbj0iZXhhY3QiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPg0KwqDCoMKgwqAJPHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BaHR0cHM6Ly9mZWRlcmF0aW9uLm9yZy9hYy90eXBlMTwvc2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj4NCsKgwqDCoMKgwqDCoMKgwqA8c2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj5odHRwczovL2ZlZGVyYXRpb24ub3JnL2FjL3R5cGUyPC9zYW1sOkF1dGhuQ29udGV4dENsYXNzUmVmPg0KwqDCoMKgwqA8L3NhbWxwOlJlcXVlc3RlZEF1dGhuQ29udGV4dD4NCgk8c2FtbHA6RXh0ZW5zaW9ucz4NCgkJPHRzLWV4dDpleHRhdHRycyB4bWxuczp0cy1leHQ9Imh0dHA6Ly9zYW1sZWlraS5mby9sb2dpbi1hdHRyLWV4dGVuc2lvbnMiPg0KCQkJPHRzLWV4dDpyZWRpcmVjdFVSST5rbGludHJhZGVtbzovLzwvdHMtZXh0OnJlZGlyZWN0VVJJPg0KwqDCoAkJPC90cy1leHQ6ZXh0YXR0cnM%2BDQoJPC9zYW1scDpFeHRlbnNpb25zPg0KPC9zYW1scDpBdXRoblJlcXVlc3Q%2B"
	     
	    //Only add the template parameter, if the Samleikin app is installed on the same device
	    let urlString = canOpenURL("samleikin://") ? "https://case1.staging.klintraft.fo/Shibboleth.sso/Login?template=\(templateParameter)"
	        : "https://case1.staging.klintraft.fo/Shibboleth.sso/Login"
	 
	    // Create an URLRequest object and use it to load the WebView
	    let url = URL(string:urlString)
	    let req = URLRequest(url: url!)
	    wv.load(req)
	    ...
	}

#### Step 2
The ViewController which contains the webview must extend WKNavigationDelegate and implement a method which checks for the requested url and opens Samleikin. The following is an example:

	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
	    let url = navigationAction.request.url
	 
	    //If the url being loaded is samleikin://, request the OS to open it and block the WebView from opening it. Otherwise let the WebView handle the navigation.
	   if (url!.scheme == "samleikin") {
	    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
	    decisionHandler(.cancel)
	    return
	   }
	   decisionHandler(.allow)
	}

#### Step 3 
N/A

#### Step 4
To register the SP app as a handler of the schema specified in the appCallbackUri, add the following to **Info.plist**:

	<array>
	 <dict>
	  <key>CFBundleTypeRole</key>
	   <string>Viewer</string>
	  <key>CFBundleURLName</key>
	  <string>{Insert your bundle identifier here}</string>
	   <key>CFBundleURLSchemes</key>
	    <array>
	     <string>{Insert your schema here}</string>
	    </array>
	 </dict>
	</array>

Expected outcome of this is:
 
* An SP app that can load a WebView that redirects to the Samleikin IdP.
* When the user presses the button that links to samleikin://, the app opens the Samleikin app.
* When the Samleikin app has processed the authentication, the SP app is opened.

### Android
#### Step 1 

Check if Samleikin is installed on the same device and add the template query parameter when requesting the protected resource
App switch is only applicable, if the Samleikin app is installed on the same device as the service provider app. To check for this we need the following:


Manifest.xml

	<manifest ...>
	 ...
	    <queries>
	        <!-- Explicit apps you know in advance about: -->
	        <package android:name="fo.gjaldstovan.samleikin"/>
	    </queries>
	 ...
	</manifest>

In the Activity that contains the WebView, the onCreate method should contain:

	...
	 
	private String loginUrl = "https://case1.staging.klintraft.fo/Shibboleth.sso/Login";
	private final String templateParameter = "PHNhbWxwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiBJRD0iZm9vIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxMi0wMS0wMVQwMDowMDowMFoiPg0KCTxzYW1scDpSZXF1ZXN0ZWRBdXRobkNvbnRleHQgQ29tcGFyaXNvbj0iZXhhY3QiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPg0KwqDCoMKgwqAJPHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BaHR0cHM6Ly9mZWRlcmF0aW9uLm9yZy9hYy90eXBlMTwvc2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj4NCsKgwqDCoMKgwqDCoMKgwqA8c2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj5odHRwczovL2ZlZGVyYXRpb24ub3JnL2FjL3R5cGUyPC9zYW1sOkF1dGhuQ29udGV4dENsYXNzUmVmPg0KwqDCoMKgwqA8L3NhbWxwOlJlcXVlc3RlZEF1dGhuQ29udGV4dD4NCgk8c2FtbHA6RXh0ZW5zaW9ucz4NCgkJPHRzLWV4dDpleHRhdHRycyB4bWxuczp0cy1leHQ9Imh0dHA6Ly9zYW1sZWlraS5mby9sb2dpbi1hdHRyLWV4dGVuc2lvbnMiPg0KCQkJPHRzLWV4dDpyZWRpcmVjdFVSST5rbGludHJhZGVtbzovLzwvdHMtZXh0OnJlZGlyZWN0VVJJPg0KwqDCoAkJPC90cy1leHQ6ZXh0YXR0cnM%2BDQoJPC9zYW1scDpFeHRlbnNpb25zPg0KPC9zYW1scDpBdXRoblJlcXVlc3Q%2B";
	 
	...
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    ...
	 
	    webView = findViewById(R.id.web_view);
	    //Shibboleth IdP uses javascript  
	    webView.getSettings().setJavaScriptEnabled(true);
	 
	    //Create a delegate WebViewClient to handle navigation events
	    webViewClient = new TSWebViewClient(this, this);
	    webView.setWebViewClient(webViewClient);
	 
	    //Only add the template parameter, if the Samleikin app is installed on the same device  
	    final String url;
	    if (this.isPackageInstalled("fo.gjaldstovan.samleikin", getPackageManager())) {
            url = loginUrl + "?template=" + templateParameter_klintrademo;
        } else {
            url = loginUrl;
        }
	 
	    //Load the WebView when the button is clicked
	    openWebViewButton = findViewById(R.id.open_web_view_button);
	    openWebViewButton.setOnClickListener(v -> {
	        webView.setVisibility(View.VISIBLE);
	        webView.loadUrl(url);
	    });
	     
	    ...
	    
	}
	
	private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
	   try {
	        packageManager.getPackageInfo(packageName, 0);
	        return true;
	   } catch (PackageManager.NameNotFoundException e) {
	        return false;
	   }
	}

#### Step 2
The WebViewClient must contain handlers to launch the Samleikin app. The following is an example:

	public class TSWebViewClient extends WebViewClient {
	 
	...
	    @Nullable
	    @Override
	    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
	         
	        if (request.getUrl().toString().equals("samleikin://")) {
	            //Stop WebView from loading "samleikin://" on the UI thread.
	            var handler = new Handler(Looper.getMainLooper());
	            handler.post(view::stopLoading);
	 
	            //Launch the Samleikin app
	            this.launchSamleikinApp();
	        }
	        return super.shouldInterceptRequest(view, request);
	    }
	 
	    private void launchSamleikinApp() {
	        try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("samleikin://"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
            catch (Exception e){
                Log.i("TSWebViewClient", "Could not start Samleikin: " + e.getMessage());
            }
	    }
	...
	 
	}

#### Step 3
N/A

#### Step 4
To register the SP app as a handler of the schema specified in the appCallbackUri, add the following to Manifest.xml:
	
	<application
	    ...>
	    <activity android:name=".{Insert name of activity with WebView here}"
	        android:launchMode="singleTop">
	        ...
	        <intent-filter>
	            <action android:name="android.intent.action.VIEW" />
	            <category android:name="android.intent.category.DEFAULT" />
	            <data android:scheme="{Insert your schema here}" />
	        </intent-filter>
	        ...
	    </activity>
	
	</application>

The launchMode="singleTop" ensures that the same activity instance is opened by the Samleikin app.
The intent filter registers the SP app and the activity containing the WebView as a handler for the given schema.


